# Copyright (C) Intel Corp.  2014-2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

"""handles synchronization of the build_root with the results directory"""
import os
import random
import socket
import subprocess
import sys
import time
import xml.sax.saxutils
import xml.etree.cElementTree as et
from utils.command import run_batch_command, rmtree
from options import Options
from project_map import ProjectMap
from project_invoke import ProjectInvoke
from dependency_graph import DependencyGraph
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


def convert_rsync_path(path, project_map=None):
    if not project_map:
        project_map = ProjectMap()

    hostname = project_map.build_spec().find("build_master").attrib["hostname"]
    repl_path = hostname + ".local::nfs/"
    if path.startswith("/mnt/jenkins/"):
        return path.replace("/mnt/jenkins/", repl_path)
    return path


class Export:
    def __init__(self, project_map=None):
        # todo: provide wildcard mechanism
        self.opt = Options()

        if project_map:
            self._project_map = project_map
        else:
            self._project_map = ProjectMap()

        self.result_path = self.opt.result_path
        if not self.result_path:
            self._dest = None
            return

        if not os.path.exists(self.result_path):
            os.makedirs(self.result_path)
            run_batch_command(["sync"])

        self._dest = self.result_path
        self.rsyncd_path = convert_rsync_path(self.result_path, project_map)
        if self.result_path:
            self._dest = self.rsyncd_path

    def export(self):
        pm = ProjectMap()
        project = pm.current_project()
        if not self.result_path:
            return

        # remove binaries that were already part of the buildroot
        rmtree("/tmp/min_build_root")
        cmd = ["rsync", "-rlpDm", "--compare-dest=/tmp/orig_build_root",
               pm.build_root(),
               "/tmp/min_build_root"]
        run_batch_command(cmd)

        cmd = ["rsync", "-rlpD",
               "--exclude='*.o'",
               "--exclude='*.a'",
               "/tmp/min_build_root/" + self.opt.arch,
               self._dest + "/" + project + "/"]
        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

        self.export_tests()

        # /tmp in some builders is not large, so clean up
        rmtree("/tmp/min_build_root")

    def export_tests(self):
        if not self.result_path:
            return

        test_path = os.path.abspath(self._project_map.build_root() + "/../test")
        if not os.path.exists(test_path):
            os.makedirs(test_path)

        cmd = ["rsync", "-rlpD",
               "--exclude='*.o'",
               "--exclude='*.a'",
               test_path,
               self._dest]

        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

        test_files = os.listdir(test_path)
        ProjectInvoke(project_map=self._project_map).set_info("test_files", test_files)

    def export_perf(self):
        if not self.result_path:
            return

        perf_path = ProjectMap().build_root()
        rsync = "rsync"
        dest = self._dest
        if not os.path.exists(perf_path):
            print("ERROR: no results to export")
            return
        cmd = [rsync, "-rlpD", "--exclude=build",
               "--exclude='*.o'",
               "--exclude='*.a'",
               perf_path,
               dest]

        try:
            run_batch_command(cmd)
        except subprocess.CalledProcessError as e:
            print("WARN: some errors copying: " + str(e))

    def import_build_root(self, arch=None):
        o = Options()
        arch = arch or o.arch
        result_path = o.result_path
        if not o.result_path:
            return
        if not os.path.exists(result_path):
            print("WARN: no build root to import, sleeping")
            time.sleep(10)
        if not os.path.exists(result_path):
            print("WARN: no build root to import: " + result_path)
            return

        pm = ProjectMap()
        project = pm.current_project()

        br = os.path.dirname(pm.build_root())
        if not os.path.exists(br):
            os.makedirs(br)

        # import only the prerequisites
        depGraph = DependencyGraph(project, self.opt)
        components = depGraph.all_builds()
        # simulated platforms require simdrm and fulsim
        if '_sim' in self.opt.hardware:
            components += [ProjectInvoke(project='sim-drm'),
                           ProjectInvoke(project='fulsim')]
        for component in components:
            # these components don't have anything in build_root, and just
            # trigger failures in rsync
            skip_components = ['mesa-perf', 'sixonix', 'steam', project]
            if component.project in skip_components:
                continue
            cmd = ["rsync", "-rlpD",
                   "--exclude='*.o'",
                   "--exclude='*.a'",
                   self._dest + "/" + component.project + "/" + arch,
                   br]
            try:
                run_batch_command(cmd)
            except subprocess.CalledProcessError as e:
                print("WARN: some errors copying: " + str(e))

        hw_needs_br_mesa = ['tgl']
        if internal_vars:
            hw_needs_br_mesa += internal_vars.hw_needs_build_root_mesa
        if self.opt.hardware in hw_needs_br_mesa:
            # use ldconfig to put br libs into ld cache
            cmd = ['sudo', 'ldconfig',
                   br + '/m64/lib',
                   br + '/m32/lib',
                   br + '/m64/lib/dri',
                   br + '/m32/lib/dri',
                   br + '/m64/lib/x86_64-linux-gnu',
                   br + '/m32/lib/i386-linux-gnu']
            run_batch_command(cmd)
            # make sure current user can write to the br after ldconfig
            try:
                cmd = ['sudo', 'chown', '-R', os.getlogin(), br]
            except OSError:
                cmd = ['sudo', 'chown', '-R', 'jenkins', br]
            run_batch_command(cmd)

        # make a copy of hard links, to be used as a filter for export
        rmtree("/tmp/orig_build_root")
        cmd = ["cp", "-al", br, "/tmp/orig_build_root"]
        run_batch_command(cmd)

        # don't want to confuse test results with any preexisting
        # files in the build root.
        test_dir = os.path.normpath(br + "/../test")
        if os.path.exists(test_dir):
            rmtree(test_dir)

    def create_failing_test(self, failure_name, output):
        o = Options()
        test_path = os.path.abspath(self._project_map.build_root() + "/../test/")
        print("ERROR: creating a failing test: "
              + failure_name + " : " + output)
        if not os.path.exists(test_path):
            os.makedirs(test_path)

        randstr = socket.gethostname() + "_" + str(random.random())[2:6]
        # filname has to begin with piglit for junit pattern match in jenkins
        # to find it.
        fh = open(test_path + "/piglit-fail-" + o.hardware + o.arch + "_" +
                  randstr + ".xml", "w")
        failure_name = failure_name + "." + o.hardware + o.arch
        fh.write("""\
<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
  <testsuite name="generated-failures" tests="1">
    <testcase classname="generated" name="{}" status="fail" time="0">
      <system-out>""".format(failure_name) + xml.sax.saxutils.escape(output) + """</system-out>
      <failure type="fail" />
    </testcase>
  </testsuite>
</testsuites>""")
        fh.close()
        Export(self._project_map).export_tests()

        # create a copy of the test xml in the source root, where
        # jenkins can access it.
        cmd = ["cp", "-a",
               self._project_map.build_root() + "/../test",
               self._project_map.source_root()]
        run_batch_command(cmd)

    def clean_orig_build_root(self):
        rmtree("/tmp/orig_build_root")

    def create_failing_test_xml(self, failure_name, output):
        o = Options()
        failure_name = failure_name + "." + o.hardware + o.arch
        root = et.Element('testsuites')
        suite = et.SubElement(
            root,
            'testsuite',
            name='generated-failures',
            tests='1')
        testcase = et.SubElement(suite, 'testcase', classname='generated',
                                 name=failure_name, status='fail', time='0')
        out = et.SubElement(testcase, 'system-out')
        out.text = xml.sax.saxutils.escape(output)
        failure = et.SubElement(testcase, 'failure', type='fail')
        return root
