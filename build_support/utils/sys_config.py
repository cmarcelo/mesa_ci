# Copyright (C) Intel Corp.  2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com
#  **********************************************************************/
import json
import hashlib
import platform
import re
import subprocess
from utils.command import run_batch_command


class SysConfig:

    _config = {
        'processor_version': None,
        'memory_speed': None,
        'memory_size': None,
        'memory_dimms': None,
        'kernel_version': None,
    }

    def __init__(self):
        self._hash = None
        self.__get_cpu_info()
        self.__get_mem_info()
        self.__get_kernel_info()

    def get(self):
        # return new dict
        return dict(self._config)

    @property
    def json(self):
        return json.dumps(self._config)

    @property
    def hash(self):
        if not self._hash:
            h = hashlib.md5()
            h.update(self.json.encode())
            self._hash = h.hexdigest()
        return self._hash

    def __get_mem_info(self):
        # dimms counted when grepping for configured freq
        num_dimms = 0
        try:
            (dmidecode_out, err) = run_batch_command(['sudo',
                                                      '/usr/sbin/dmidecode',
                                                      '-t', 'memory'],
                                                     streamedOutput=False,
                                                     quiet=True)
        except subprocess.CalledProcessError as e:
            print("Unable to determine system memory information, "
                  "dmidecode failed:")
            print(e)
            return

        # use a set to store all freqs for memory, if there is a difference
        # between current and max for anything, then len(set) > 1
        mem_freqs = set()
        speed_re = re.findall('Configured Memory Speed: \\d.* MT',
                              dmidecode_out.decode())
        if not speed_re:
            print("Unable to determine system memory frequency in dmidecode "
                  "output")
            return
        for line in speed_re:
            freq = line.split(' ')[3]
            mem_freqs.add(freq)
            num_dimms += 1
        if len(mem_freqs) > 1:
            raise Exception("ERROR: detected multiple 'cofigured memory "
                            "speeds' for system memory")
        if not mem_freqs:
            raise Exception("ERROR: could not find system memory frequency")
        self._config['memory_speed'] = mem_freqs.pop()
        self._config['memory_dimms'] = num_dimms

        size_re = re.findall('Size: \\d.*B', dmidecode_out.decode())
        total_size = 0
        for line in size_re:
            try:
                total_size += int(line.split(' ')[1])
            except ValueError:
                print("unable to get memory size from: " + line)
        self._config['memory_size'] = total_size

    def __get_cpu_info(self):
        (out, err) = run_batch_command(['sudo', '/usr/sbin/dmidecode', '-s',
                                        'processor-version'],
                                       streamedOutput=False, quiet=True)
        p_ver = out.decode()
        if p_ver:
            self._config["processor_version"] = out.decode().rstrip()

    def __get_kernel_info(self):
        self._config['kernel_version'] = platform.release()
