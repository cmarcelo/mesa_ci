# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import hashlib
import multiprocessing
import os
import sys
import socket
import time
from .command import run_batch_command
from options import Options
from project_map import ProjectMap
from urllib.request import urlopen
from export import Export
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


def git_clean(src_dir):
    savedir = os.getcwd()
    os.chdir(src_dir)
    run_batch_command(["git", "clean", "-xfd"])
    run_batch_command(["git", "reset", "--hard", "HEAD"])
    if os.path.exists('.git/rebase-apply'):
        run_batch_command(["git", "am", "--abort"])
    os.chdir(savedir)


def cpu_count():
    cpus = multiprocessing.cpu_count() + 1
    if cpus > 18:
        cpus = 18
    return cpus


class DefaultTimeout:
    def __init__(self, options=None):
        self._options = options
        if not options:
            self._options = Options()

    def GetDuration(self):
        """by default, components should finish in under 15 minutes.
        For daily builds, 60 minutes is acceptable."""

        if self._options.type == "daily" or self._options.type == "release":
            return 120
        if self._options.hardware == "byt":
            # bay trail takes 15min to rsync to /tmp on the sdcard
            return 30
        return 25


def is_soft_fp64(hardware):
    """ Determine if hardware uses a software fp64 implementation"""
    if not hardware:
        return False
    soft_fp64_hw = {'icl', 'icl_iris', 'tgl', 'tgl_sim'}
    if internal_vars:
        soft_fp64_hw.update(internal_vars.soft_fp64_hw)
    return hardware in soft_fp64_hw


class NoConfigFile(Exception):
    def __str__(self):
        return ("ERROR: No conf file found. Please create a conf for this "
                "hardware platform under <mesa_jenkins>/<test>/<hardware>."
                "conf")


def get_conf_file(hardware, arch, project="piglit-test", _base_dir=None):
    pm = ProjectMap()
    base_dir = _base_dir
    if not base_dir:
        base_dir = pm.source_root()
    conf_dir = base_dir + "/" + project + "/"
    conf_file = conf_dir + "/" + hardware + arch + ".conf"
    if not os.path.exists(conf_file):
        conf_file = conf_dir + "/" + hardware + ".conf"

    if os.path.exists(conf_file):
        return conf_file

    if "gt" in hardware:
        # we haven't found a sku-specific conf, so strip the gtX
        # strings off of thef hardware and try again.
        return get_conf_file(hardware[:3], arch, project, _base_dir)

    if _base_dir:
        raise NoConfigFile
    # Try again but use internal conf path
    return get_conf_file(hardware, arch, project,
                         base_dir + "/repos/mesa_ci_internal/" + "/")


def mesa_version(env=None):
    br = ProjectMap().build_root()
    wflinfo = br + "/bin/wflinfo"
    if not env:
        env = {}
    env.update({
        'LD_LIBRARY_PATH': ':'.join([
            get_libdir(),
            get_libgl_drivers(),
            os.path.join(br, 'lib', 'piglit', 'lib'),
        ]),
        "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
    })
    (out, _) = run_batch_command([wflinfo,
                                 "--platform=gbm", "-a", "gl"],
                                 streamedOutput=False, env=env)
    for a_line in out.splitlines():
        a_line = a_line.decode()
        if "OpenGL version string" not in a_line:
            continue
        tokens = a_line.split(":")
        assert len(tokens) == 2
        version_string = tokens[1].strip()
        version_tokens = version_string.split()
        assert len(version_tokens) >= 3
        for token in version_tokens:
            if '.' in token:
                # token is a version string, eg "20.1"
                return token
        assert False


def _system_dirs():
    """Returns the correct lib prefix for debian vs non-debian."""
    lib_dirs = []
    if Options().arch == "m32":
        lib_dirs.append("lib32")
        lib_dirs.append("lib/i386-linux-gnu")
    else:
        lib_dirs.append("lib64")
        lib_dirs.append("lib/x86_64-linux-gnu")
    # add 'lib' here since it can cause conflicts on some distros (Arch)
    # where */lib contains m64 binaries
    lib_dirs.append('lib')
    return lib_dirs


def get_package_config_path():
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    pkg_configs = ([os.path.join(build_root, l, 'pkgconfig') for l in lib_dirs]
                   + [os.path.join('/usr', l, 'pkgconfig') for l in lib_dirs])
    # meson has deprecated support for specifying paths in pkg_config_path more
    # than once, so remove any duplicates while maintaining the same ordering
    pkg_configs = list(dict.fromkeys(pkg_configs))
    return ':'.join(pkg_configs)


def get_libgl_drivers():
    """Get the correct drivers dir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l, 'dri') for l in lib_dirs] +
        [os.path.join('/usr', l, 'dri') for l in lib_dirs])


def get_libdir():
    """Get the correct libdir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l) for l in lib_dirs] +
        [os.path.join('/usr', l) for l in lib_dirs])


class NullInvoke:
    """masquerades as an invoke object, so the main routine can post
    results even if there is no server to post to"""
    def __init__(self):
        pass

    def set_info(self, *args):
        pass

    def set_status(self, *args):
        pass


def write_pid(pidfile):
    """Write the PID file."""
    with open(pidfile, 'w') as f:
        f.write(str(os.getpid()))


class TimeoutException(Exception):
    def __init__(self, msg):
        Exception.__init__(self)
        self._msg = msg

    def __str__(self):
        return self._msg


def signal_handler(signum, frame):
    raise TimeoutException("Fetch timed out.")


def signal_handler_quit(signum, frame):
    sys.exit(-1)


def file_checksum(fname):
    """ Generate md5 checksum for given file """
    with open(fname, 'rb') as f:
        return hashlib.md5(f.read()).hexdigest()

def fail_and_reboot(failure_name, msg, project_map=None):
    """ Create a custom failing test. Reboot if calling this function on a CI tester """
    if not project_map:
        project_map = ProjectMap()
    hostname = socket.gethostname()
    Export(project_map).create_failing_test(failure_name, msg)
    # trigger reboot
    label = None
    if 'otc-gfx' in hostname:
        label = hostname[len('otc-gfxtest-'):]
    elif 'mesa-perf' in hostname:
        label = hostname[len('mesa-perf-'):]

    if not label:
        print("ERROR: unable to find jenkins label in hostname: " + hostname)
        return
    print("ERROR: system must be rebooted.")
    server = project_map.build_spec().find("build_master").attrib["host"]
    url = "http://" + server + "/job/reboot_single/buildWithParameters?token=noauth&label=" + label
    print("opening: " + url)
    urlopen(url)
    print("sleeping to allow reboot job to be scheduled.")
    time.sleep(120)

def get_blacklists():
    """
    Collect list of blacklist files for given project (e.g. 'glcts-test')
    """
    pm = ProjectMap()
    opts = Options()
    bd = pm.project_build_dir()
    project = pm.current_project()
    # don't strip away _iris from hardware
    hw_prefix = ""
    iris_only = []
    if internal_vars:
        iris_only += internal_vars.iris_only
    use_iris = False
    if '_iris' not in opts.hardware:
        hw_prefix = opts.hardware[:3]
    else:
        use_iris = True
    use_iris = use_iris or opts.hardware in iris_only
    possible_blacklists = [bd + opts.hardware + "_blacklist.conf",
                           bd + hw_prefix + "_blacklist.conf",
                           bd + opts.arch + "_blacklist.conf",
                           bd + hw_prefix + '_' + opts.arch + "_blacklist.conf",
                           bd + "blacklist.conf",
                           pm.source_root() + ("/repos/mesa_ci_internal/"
                                               + project
                                               + "/blacklist.conf"),
                           pm.source_root() + ("/repos/mesa_ci_internal/"
                                               + project + '/'
                                               + opts.hardware
                                               + "_blacklist.conf")
                           ]
    if is_soft_fp64(opts.hardware) and opts.type != 'daily':
        possible_blacklists.append(bd + 'soft-fp64_blacklist.conf')
    if opts.type != "daily" and not opts.retest_path:
        possible_blacklists.append(bd + 'non-daily_blacklist.conf')
    if use_iris:
        possible_blacklists.append(bd + 'iris_blacklist.conf')
    if opts.hardware.endswith('_sim'):
        possible_blacklists += [
            bd + 'sim_blacklist.conf',
            pm.source_root() + ("/repos/mesa_ci_internal/"
                                + project
                                + "/sim_blacklist.conf")
        ]

    blacklist_files = []
    for file in possible_blacklists:
        if os.path.exists(file) and file not in blacklist_files:
            blacklist_files.append(file)
    print('Using blacklists:\n' + '\n'.join(blacklist_files))
    return blacklist_files
