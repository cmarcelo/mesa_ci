# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import os
from utils.command import run_batch_command
from export import Export
from project_map import ProjectMap
from builders import AutoBuilder, MesonBuilder


class AutotoolsCrucibleBuilder(AutoBuilder):
    def __init__(self):
        self._pm = ProjectMap()
        mesa_lib = "MESA_LDFLAGS=-L" + self._pm.build_root() + "/lib"
        AutoBuilder.__init__(self, configure_options=[mesa_lib])
        self._build_dir = self._src_dir

    def build(self):
        AutoBuilder.build(self)
        bin_dir = self._build_root + "/bin/"
        if not os.path.exists(bin_dir):
            os.makedirs(bin_dir)
        run_batch_command(["cp", "-a", "-n",
                           self._build_dir + "/bin/crucible", bin_dir])
        run_batch_command(["cp", "-a", "-n",
                           self._build_dir + "/data/", self._build_root])
        Export().export()

class MesonCrucibleBuilder(MesonBuilder):
    def __init__(self):
        MesonBuilder.__init__(self, install=False)

    def build(self):
        MesonBuilder.build(self)
        bin_dir = self._build_root + "/bin/"
        if not os.path.exists(bin_dir):
            os.makedirs(bin_dir)
        run_batch_command(["cp", "-a", "-n",
                           self._build_dir + "/crucible", bin_dir])
        run_batch_command(["cp", "-a", "-n",
                           self._build_dir + "/data/", bin_dir + 'data'])
        Export().export()

def CrucibleBuilder():
    pm = ProjectMap()
    project = pm.current_project()
    src_dir = pm.project_source_dir(project)
    if os.path.exists(os.path.join(src_dir, "meson.build")):
        return MesonCrucibleBuilder()
    else:
        return AutotoolsCrucibleBuilder()
