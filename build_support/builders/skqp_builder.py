# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
from __future__ import print_function
import glob
import json
import os
import subprocess
from options import Options
from project_map import ProjectMap
from utils.command import run_batch_command, rmtree
from export import Export
from utils.utils import (cpu_count, git_clean)
from repo_set import checkout_externals, external_to_repo


def skqp_external_revisions(project, revisions_dict=None):
    if revisions_dict is None:
        revisions_dict = {}
    src_dir = ProjectMap().project_source_dir(project)
    deps_file = src_dir + '/DEPS'
    deps = skqp_get_deps_from_file(deps_file)
    for orig_dep_name, url in deps.items():
        # Ignore any values that are really dicts and not a url
        if type(url) == dict:
            continue
        dep_name = orig_dep_name.split('/')[-1]
        revision = url.split('@')[-1]
        if dep_name in external_to_repo:
            dep_name = external_to_repo[dep_name]
        if dep_name in revisions_dict:
            revisions_dict[dep_name] = [revisions_dict[dep_name], revision]
        else:
            revisions_dict[dep_name] = revision
    fc_path = (ProjectMap().project_source_dir(project)
               + '/platform_tools/android/apps/skqp/src'
               + '/main/assets/files.checksum')
    with open(fc_path, 'r') as f:
        assets_ver = f.readline().rstrip()
    # assets version is a tag with a sha-like name which confuses git, so
    # 'tags' is prepended to help out git
    revisions_dict['skqp_assets'] = 'tags/' + assets_ver
    return revisions_dict


def skqp_get_deps_from_file(file_path):
    """ Return dict of dependencies listed in skqp DEPS file at
    given file_path """
    deps = {}
    exec(open(file_path).read(), deps)
    return deps['deps'].copy()


def get_external_revisions(revisions_dict=None):
    return skqp_external_revisions(project="skqp",
                                   revisions_dict=revisions_dict)


class SkqpBuilder():

    def __init__(self, extra_definitions=None, install=True):
        self._options = Options()
        self._pm = ProjectMap()
        self._install = install
        self._extra_definitions = extra_definitions or []
        self.tests = []  # List of tests to run
        self.gtests = ['skqp']

        self.project = self._pm.current_project()

        self._src_dir = self._pm.project_source_dir(self.project)
        self._build_root = self._pm.build_root()
        self._build_dir = os.path.join(
            self._src_dir, '_'.join(['build', self.project,
                                     self._options.arch]))

    def build(self):
        rmtree(self._src_dir + "/third-party")
        returnto = os.getcwd()
        dest = self._build_root + '/opt/skqp'
        assets = (self._pm.project_source_dir()
                  + '/../skqp_assets')
        if not os.path.exists(dest):
            os.makedirs(dest)
        os.chdir(self._src_dir)

        # Patch in local mirror of dependencies rather than fetching them
        # externally. Dependencies should be placed in 'repos' dir by
        # fetch_sources (or something similar).
        # Use skqp's git-sync-deps script after patching DEPS to pull in
        # dependencies.
        deps_file = self._src_dir + '/DEPS'
        deps_patched = False
        with open(deps_file, 'r') as f:
            deps_patched = f.readlines()[0].startswith('#patched')
        if not deps_patched:
            deps = skqp_get_deps_from_file(deps_file)
            for orig_dep_name, url in deps.items():
                # the deps dict in DEPS may contain sub-dicts that we don't
                # care about, so remove it
                if type(deps[orig_dep_name]) == dict:
                    deps.pop(orig_dep_name, None)
                    continue
                dep_name = orig_dep_name.split('/')[-1]
                # some deps are in the local repos under slightly different
                # names..
                if dep_name in external_to_repo:
                    dep_name = external_to_repo[dep_name]
                deps[orig_dep_name] = (self._pm.project_source_dir()
                                       + '/../'
                                       + dep_name
                                       + '@' + url.split('@')[1])
            with open(deps_file, 'w') as f:
                f.write('#patched\n')
                f.write('use_relative_paths = True\n')
                f.write('deps = ' + json.dumps(deps) + '\n')
                f.write('\nrecursedeps = [ "common" ]')
        revisions = get_external_revisions()
        external_dir = (self._src_dir + "/third_party/externals/{}/")
        checkout_externals(project='skqp', revisions=revisions,
                           external_dir_format=external_dir)
        # buildtools and common actually belong in the skqp repo root..
        for d in ['common', 'buildtools']:
            if not os.path.exists(self._src_dir + '/' + d):
                os.symlink(self._src_dir + '/third_party/externals/' + d,
                           self._src_dir + '/' + d)
        if not os.path.exists(self._src_dir + '/buildtools/linux64/gn'):
            os.symlink(self._src_dir + '/third_party/externals/skqp_assets/bin/gn',
                       self._src_dir + '/buildtools/linux64/gn')
        # This script copies dependency repos to third_party/external
        run_batch_command(['python', 'tools/skqp/setup_resources'])
        # apply patches if they exist
        for patch in sorted(glob.glob(os.path.join(
                            self._pm.project_build_dir(),
                            '*.patch'))):
            try:
                run_batch_command(['git', 'apply', patch])
            except subprocess.CalledProcessError:
                print('WARN: failed to apply patch: {}'.format(patch))

        cmd = ' '.join([self._src_dir + '/buildtools/linux64/gn', 'gen',
                        self._build_dir,
                        ' --args=\'cc=\"ccache clang\" '
                        + 'cxx=\"ccache clang++\"\''])
        if os.path.exists(self._build_dir):
            rmtree(self._build_dir)
        run_batch_command(cmd, shell=True)
        run_batch_command(['ninja', '-j', str(cpu_count()), '-C',
                           self._build_dir, 'skqp'])
        run_batch_command(["rm", "-r", self._build_dir + "/obj"])
        run_batch_command(['rsync', '-rlptD', self._build_dir + '/', dest])
        # install assets
        run_batch_command(['rsync', '-rlptD', assets + '/', dest + '/assets'])
        # install resources (needed by some tests)
        run_batch_command(['rsync', '-rlptD', self._src_dir + '/resources', dest])

        os.chdir(returnto)

        Export().export()

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert not os.path.exists(self._build_dir)

    def test(self):
        pass
