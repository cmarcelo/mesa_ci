# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/

import hashlib
import json
import os
import shutil
import signal
import subprocess
import sys
import time
# TODO: cElementTree is deprecated
import xml.etree.cElementTree as et

import git

from project_map import ProjectMap
from options import Options
from utils.command import run_batch_command


external_to_repo = {"spirv-headers": "spirvheaders",
                    "spirv-tools": "spirvtools",
                    "googletest": "gtest",
                    "common": "google_common",
                    "buildtools": "google_buildtools"}
repo_to_external = {v: k for (k, v) in external_to_repo.items()}


def checkout_externals(project, revisions, external_dir_format):
    pm = ProjectMap()
    for external_repo, rev in revisions.items():
        external_dir = external_dir_format.format(external_repo)
        external_proj = external_repo
        if external_repo in repo_to_external:
            external_proj = repo_to_external[external_repo]
            external_dir = external_dir_format.format(external_proj)
        print("Checking out external: {}".format(external_proj))
        if not os.path.exists(external_dir):
            # clone the project from the local cache in repos
            git.Repo.clone_from(pm.project_source_dir(external_repo),
                                external_dir)
            repo = git.Repo(external_dir)
            with repo.remotes[0].config_writer as c:
                c.config.set_value('remote \"origin\"',
                                   'fetch',
                                   '+refs/*:refs/remotes/*')

        savedir = os.getcwd()
        os.chdir(external_dir)
        run_batch_command(["git", "clean", "-xfd"])
        run_batch_command(["git", "reset", "--hard", "HEAD"])
        os.chdir(savedir)
        repo = git.Repo(external_dir)
        repo.remotes[0].fetch(verbose=False)
        repo.git.checkout(rev, force=True)


class _ProjectBranch:
    def __init__(self, projectName):
        # default to master branch
        self.branch = "origin/master"
        self.name = projectName
        self.sha = None
        self.trigger = False


class RevisionSpecification:
    def __init__(self, revisions=None, repo_set=None, only_projects=None, project_map=None):
        # key is project, value is revision
        if revisions is not None:
            assert isinstance(revisions, dict)
            self._revisions = revisions
        else:
            self._revisions = {}
            if repo_set is None:
                repo_set = RepoSet(project_map=project_map)
            # By default, take all projects
            if only_projects is None:
                only_projects = repo_set.projects()
            for p in only_projects:
                try:
                    repo = repo_set.repo(p)
                    rev = repo.git.rev_parse("HEAD", short=True)
                except:
                    continue
                self._revisions[p] = rev

    @classmethod
    def from_xml_file(cls, filename):
        elem = et.ElementTree(file=filename).getroot()
        if elem.tag != 'RevSpec':
            elem = elem.find('RevSpec')
            assert elem is not None
        inst = cls(revisions=elem.attrib)
        return inst

    @classmethod
    def from_cmd_line_param(cls, params):
        return cls(revisions=dict(p.split('=') for p in params))

    def to_cmd_line_param(self):
        revs = []
        for project, revision in self._revisions.items():
            revs.append(project + "=" + revision)
        revs.sort()
        return " ".join(revs)

    def to_elementtree(self):
        elem = et.Element('RevSpec')
        for n, h in sorted(self._revisions.items(), key=lambda x: x[0]):
            elem.set(n, h)
        return et.ElementTree(elem)

    def __str__(self):
        return et.tostring(self.to_elementtree().getroot(),
                           encoding="UTF-8").decode()

    def checkout(self):
        repo_set = RepoSet()
        for (project, revision) in self._revisions.items():
            project_repo = repo_set.repo(project)
            project_repo.git.checkout(["-f", revision])

    def revision(self, project):
        return self._revisions[project]

    def set_revision(self, project, rev):
        assert(project in self._revisions)
        self._revisions[project] = rev

    def projects(self):
        return self._revisions.keys()

    def get_branchpoint(self, project, trunk="origin/master"):
        """ Get the branch point for the currently checked out revision in the
        given project, in the given trunk 'branch' (defaults to
        'origin/master') of the project.
        Returns None if unable to find one.
        """
        mesa_repo = git.Repo(ProjectMap().project_source_dir("mesa"))
        branch = self._revisions[project]
        bp = None
        try:
            bp = mesa_repo.git.merge_base(trunk, branch)
        except git.exc.GitCommandError as e:
            print(e)
            print("ERROR: unable to retrieve branch point between "
                  f" {trunk} and {branch}")
        return bp


class BranchSpecification:
    """This class tracks a "branch set" in the build's git repositories
    which define a single logical build.  A change to any of the
    branches will result in a world build.

    """
    def __init__(self, branch_tag, repo_set=None):
        self._project_branches = {}
        self.name = branch_tag.attrib["name"]
        self.project = branch_tag.attrib["project"]
        self.priority = branch_tag.attrib.get('priority')
        try:
            self.priority = int(self.priority)
        except (ValueError, TypeError):
            self.priority = 3
        if not repo_set:
            repo_set = RepoSet()
        self._repos = repo_set

        # by default, all repos are at origin/master
        for name in repo_set.projects():
            pb = _ProjectBranch(name)
            self._project_branches[name] = pb
            pb.branch = repo_set.branch(name)

        # override the defaults
        for a_project in branch_tag:
            name = a_project.tag
            if not name in self._project_branches:
                # repo unavailable
                continue
            self._project_branches[name].trigger = True
            # allow the spec to set a "stable" branch that won't trigger
            if "trigger" in a_project.attrib:
                trigger = (a_project.attrib["trigger"] == "true")
                self._project_branches[name].trigger = trigger
            if "branch" in a_project.attrib:
                self._project_branches[name].branch = a_project.attrib["branch"]

        invalid = []
        for (name, branch) in self._project_branches.items():
            repo = repo_set.repo(branch.name)
            if not repo:
                continue
            try:
                branch.sha = repo.commit(branch.branch).hexsha
            except:
                pass

    def update_commits(self, limit_to_repos=None):
        # get the current commit for each project
        self._repos.fetch(only_projects=limit_to_repos)
        for (_, branch) in self._project_branches.items():
            repo = self._repos.repo(branch.name)
            branch.sha = repo.commit(branch.branch).hexsha

    def set_revisions(self, project_dict):
        invalid_projects = []
        for (k,v) in project_dict.items():
            if v:
                continue
            if k not in self._project_branches:
                invalid_projects.append(k)
            else:
                project_dict[k] = self._project_branches[k].branch
        for p in invalid_projects:
            del(project_dict[p])
        
    def needs_build(self):
        # checks the commits on the branch repos to see if they have
        # been updated.
        for (_, branch) in self._project_branches.items():
            if not branch.trigger:
                continue
            repo = self._repos.repo(branch.name)
            try:
                hexsha = repo.commit(branch.branch).hexsha
            except Exception as e:
                print("WARN: Branch does not exist: {}\n"
                      "WARN: Perhaps it was deleted and build_specification "
                      "was not updated? Full exception:".format(branch.branch))
                print(e)
                continue
            if branch.sha != hexsha:
                print(f"*** change in branch: {branch.name}, old branch sha: "
                      f"{branch.sha}, new branch sha: {hexsha}")
                return branch.name + "=" + repo.git.rev_parse(hexsha, short=True)
        return False

    def checkout(self):
        """checks out the specified branches for each repository in the branch
        set """
        for (name, branch) in self._project_branches.items():
            repo = self._repos.repo(name)
            success = False
            attempt = 0
            while not success and attempt < 10:
                try:
                    attempt += 1
                    repo.git.reset("--hard")
                    repo.git.checkout(["-f", branch.branch])
                    success = True
                except:
                    print("Encountered error checking out")
                    time.sleep(10)
                    run_batch_command(["rm", "-f", repo.working_tree_dir + "/.git/index.lock"])


class TimeoutException(Exception):
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg


def is_build_lab():
    buildspec = ProjectMap().build_spec()
    master_host = buildspec.find("build_master").attrib["hostname"]
    # Sometimes the lab network is under load, give adequate time for a ping
    # response
    cmd = ["ping", "-c", "1", "-w", "5", "-q", master_host + ".local"]
    if os.name == "nt":
        master_host = buildspec.find("build_master").attrib["host"]
        cmd = ["ping", "-w", "5", master_host]
    p = subprocess.Popen(cmd,
                         stderr=open(os.devnull, "w"), stdout=open(os.devnull, "w"))
    p.communicate()
    if p.returncode:
        # error from ping: not in build lab
        return False
    return True


class RepoNotCloned(Exception):
    def __init__(self, repo):
        Exception.__init__(self, "Repo should be cloned first: %s" % repo)


class RepoSet:
    """this class represents the set of git repositories which are
    specified in the build_specification.xml file."""
    def __init__(self, repos_root=None, use_cache=True, mirror=False, project_map=None):
        """
        Keyword arguments:
        repos_root  -- Destination for git repositories (default is ./repos/)
        use_cache   -- Cloning/fetching will happen from
                       build_master (default True)
        mirror      -- Pass --mirror when creating clones (default False)
        """
        if project_map:
            self.project_map = project_map
        else:
            self.project_map = ProjectMap()
        self.buildspec = self.project_map.build_spec()
        self._repos = {}
        # key is repo name, value is dictionary of remote name => remote object
        self._remotes = {}
        # key is repo name, value is the default branch for the repo
        # (usually master)
        self._branches = {}
        self._master_host = self.buildspec.find("build_master").attrib["hostname"]
        self._use_cache = use_cache
        self._mirror = mirror


        self._repos_root = self.project_map.source_root() + "/repos/"
        if repos_root:
            self._repos_root = repos_root

        # Validate any existing repos under repos_root and add them to
        # this object
        repos = self.buildspec.find("repos")
        for tag in repos:
            project = tag.tag
            project_repo_dir = self._repos_root + "/" + project
            if os.path.exists(project_repo_dir):
                try:
                    repo = git.Repo(project_repo_dir)
                    self._repos[project] = repo
                    self._remotes[project] = {}
                    for remote in repo.remotes:
                        self._remotes[project][remote.name] = remote
                    branch = "origin/master"
                    if "branch" in tag.attrib:
                        branch = tag.attrib["branch"]
                    self._branches[project] = branch
                except git.InvalidGitRepositoryError:
                    # Something broke with the repo, so remove it and re-clone
                    print("INFO: Repo path is not a valid git repo: %s. Removing..."
                          % project_repo_dir)
                    shutil.rmtree(project_repo_dir)

    def clone(self, limit_to_repos=None):
        """ Clone all repos specified in build_specification.xml
            Note: This method does *not* fetch remotes """
        build_lab = is_build_lab()
        # systems not in build lab should not use cache (e.g. the system is an
        # external developer system).
        use_cache = self._use_cache and build_lab
        if use_cache:
            if os.name == "nt":
                git_cache = ("git://" +
                                   self.buildspec.find("build_master").attrib["host"] +
                                   "/git/")
            else:
                git_cache = ("git://" +
                                   self.buildspec.find("build_master").attrib["hostname"] +
                                   ".local/git/")

        cloned_repo = False
        if not os.path.exists(self._repos_root):
            os.makedirs(self._repos_root)
        attempts = 1
        if build_lab and use_cache:
            attempts = 10
        # Parse buildspec in case there were changes since RepoSet
        # was initialized
        self.buildspec = ProjectMap().build_spec()

        repos = self.buildspec.find("repos")
        # clone all the repos into repos_root
        for tag in repos:
            repo_name = tag.tag
            repo_dir = self._repos_root + tag.tag
            repo = None
            # Builders/testers should clone from master's cache,
            # everything else will clone from upstream.
            url = tag.attrib["repo"]
            if use_cache:
                url = git_cache + repo_name
            branch = "origin/master"
            if "branch" in tag.attrib:
                branch = tag.attrib["branch"]
            # Try to use existing repo_dir if there is one. If it's invalid
            # and not explicitly disabled, then remove it so that a re-clone
            # can be attempted
            if os.path.exists(repo_dir) and not os.path.exists(repo_dir +
                                                               "/do_not_use"):
                try:
                    repo = git.Repo(repo_dir)
                except git.InvalidGitRepositoryError:
                    # Something broke with the repo, so remove it and re-clone
                    print("INFO: Repo path exists but is not a valid git "
                          "repo: %s. Attempting to repair... " % repo_dir)
                    shutil.rmtree(repo_dir)
            # Clone any repos that do not exist on disk
            if not os.path.exists(repo_dir):
                if limit_to_repos:
                    if repo_name not in limit_to_repos:
                        continue

                success = False
                for attempt in range(0, attempts):
                    if attempt > 0:
                        time.sleep(10)
                    try:
                        print("Attempting clone of %s" % url)
                        git.Repo.clone_from(url, repo_dir,
                                            mirror=self._mirror)
                        if self._mirror:
                            run_batch_command(['touch',
                                               repo_dir + '/git-daemon-export-ok'])
                        success = True
                        cloned_repo = True
                        break
                    except:
                        print("WARN: unable to clone repo: %s\n"
                              "Exception text: %s" % (url, sys.exc_info()[0]))
                # If the repo is not clone-able, do_not_use is used to disable
                # it from any future attempts to clone/fetch
                if not success and not build_lab:
                    os.makedirs(repo_dir + "/do_not_use")
                    continue
            if os.path.exists(repo_dir + "/do_not_use"):
                continue
            try:
                repo = git.Repo(repo_dir)
            except git.InvalidGitRepositoryError:
                if not build_lab:
                    os.makedirs(repo_dir + "/do_not_use")
                print("WARNING: Unable to clone repo: %s" % repo_name)
            # Systems not using cache (e.g. cloning from an external remote
            # should add all remotes to the repo.
            if not use_cache:
                for a_remote in tag.findall("remote"):
                    remote_name = a_remote.attrib["name"]
                    remote_repo = a_remote.attrib["repo"]
                    if not remote_name or not remote_repo:
                        continue
                    remote = None
                    try:
                        remote = repo.remote(name=remote_name)
                        # verify that remote url is set to the value in
                        # build_specification
                        if remote_repo not in remote.urls:
                            print("INFO: Updating changed remote url for %s"
                                  % remote_name)
                            for url in remote.urls:
                                remote.set_url(remote_repo, old_url=url)
                        # Remote does not exist, so add it
                        # Note: remotes are added to the repo with the
                        # following fetch refspec:
                        #    +refs/heads/*:refs/<remote_name>/*
                    except ValueError:
                        remote = repo.create_remote(remote_name, remote_repo)
                        with remote.config_writer as c:
                            c.config.set_value('remote \"' + remote_name
                                               + '\"', 'fetch',
                                               '+refs/heads/*:refs/'
                                               + remote_name + '/*')
            else:
                # For systems that will be fetching from build master's git
                # cache, add the appropriate fetch refspec so that refs are
                # mapped to refs/remotes/*
                origin = repo.remote("origin")
                assert origin is not None
                with origin.config_writer as c:
                    c.set('fetch', '+refs/*:refs/remotes/*')
                repo.git.config('--local', '--add', 'remote.origin.fetch',
                                '+refs/heads/*:refs/remotes/origin/*')
            # Store repo, branch, and remote object(s)
            self._repos[repo_name] = repo
            self._branches[repo_name] = branch
            self._remotes[repo_name] = {}
            tag_remotes = [x.attrib["name"] for x in tag.findall("remote")]
            for remote in repo.remotes:
                self._remotes[repo_name][remote.name] = remote
                # Clean out any remotes that are no longer in the buildspec
                if remote.name == "origin":
                    continue
                if remote.name not in tag_remotes:
                    print("Remote does not exist in buildspec anymore, "
                          "deleting it: %s"
                          % remote.name)
                    repo.delete_remote(remote.name)
        return cloned_repo

    def repo(self, project_name):
        return self._repos[project_name]

    def branch(self, project_name):
        return self._branches[project_name]

    def projects(self):
        return self._repos.keys()

    def alarm(self, secs):
        if os.name != "nt":
            signal.alarm(secs)   # 5 minutes

    def fetch(self, only_projects=None, gc=False):
        if not only_projects:
            only_projects = {}

        def signal_handler(signum, frame):
            raise TimeoutException("Fetch timed out.")

        self._master_host = self.buildspec.find("build_master").attrib["hostname"]
        buildspec_repos = self.buildspec.find("repos")
        for tag in buildspec_repos:
            repo_name = tag.tag
            if only_projects and repo_name not in only_projects:
                continue
            # Make sure the repo we're fetching has been cloned first:
            if repo_name not in self._repos.keys():
                raise RepoNotCloned(repo_name)
            repo = self._repos[repo_name]
            if (only_projects and
                only_projects[repo_name] and
                # not a branch
                '/' not in only_projects[repo_name]):
                try:
                    repo.commit(only_projects[repo_name])
                    # desired commit is already in the repo
                    continue
                except:
                    # need to fetch
                    pass

            # Removing gc.log is relevant only for systems where the repo
            # is not bare:
            if repo.working_tree_dir:
                garbage_collection_fail = (repo.working_tree_dir +
                                           "/.git/gc.log")
                if os.path.exists(garbage_collection_fail):
                    run_batch_command(["rm", "-f", garbage_collection_fail])
            if gc:
                print("calling git gc on: " + repo_name)
                try:
                    repo.git().gc()
                except Exception as e:
                    print("ERROR: git repo is corrupt, removing: %s" %
                          repo.working_tree_dir)
                    print(e)
                    run_batch_command(["rm", "-rf", repo.working_tree_dir])
                    raise

            if os.name != "nt":
                signal.signal(signal.SIGALRM, signal_handler)
            # Iterate through all remotes and fetch them
            target_remotes = repo.remotes
            target_commit = ""
            if repo_name in only_projects:
                target_commit = only_projects[repo_name]
            target_remotes = {}
            if target_commit and '/' in target_commit:
                target_remotes = {"origin" : True}
                branch_remote = target_commit.split("/")[0]
                target_remotes[branch_remote] = True
                
            for remote in repo.remotes:
                if target_remotes and remote.name not in target_remotes:
                    # spec asks for a branch that is not in the remote
                    continue
                print("fetching " + remote.url)
                # 4 attempts
                success = False
                for _ in range(1, 4):
                    try:
                        self.alarm(300)   # 5 minutes
                        remote.fetch(['--tags', '--force'], verbose=False)
                        self.alarm(0)
                        success = True
                        break
                    except git.GitCommandError as e:
                        # print first 10 lines of stderr
                        msg = '\n'.join(str(e).split('\n')[:10])
                        print("error fetching: %s" % msg)
                    except AssertionError as e:
                        print("assertion while fetching: %s" % str(e))
                    except TimeoutException as e:
                        print(str(e))
                    except Exception as e:
                        print(str(e))
                    finally:
                        self.alarm(0)
                        time.sleep(1)
                if not success:
                    print("Failed to fetch remote, ignoring: %s" % remote)
                if target_commit and '/' not in target_commit:
                    try:
                        repo.commit(target_commit)
                        # already fetched the specific rev
                        break
                    except:
                        # need to fetch more
                        pass
                    

    def branch_missing_revisions(self, deps=None):
        """provides the revisions which are on master but are not on the
        current branch(es).  This information can be used to filter
        out known test failures that only exist on the branch

        """
        if deps:
            projects = deps.all_sources()
        else:
            projects = self.projects()
        revs = []
        for project in projects:
            repo = self.repo(project)
            # branches can be long-lived: eg mesa_10.4.  300 commits
            # on the branch is long enough for 10.4
            try:
                branch_commits = [commit.hexsha for commit in repo.iter_commits(max_count=1200)]
            except:
                print("Warning: Unable to find commit in " + project + ", make sure your repos are up to date!")
                continue
            tmp_revs = []

            # branchs can be a long time in the past.  For 10.4, there
            # have been more than 1000 commits since the branch point.
            try:
                for master_commit in repo.iter_commits(self._branches[project], max_count=8000):
                    hexsha = master_commit.hexsha
                    if hexsha not in branch_commits:
                        tmp_revs.append(hexsha)
                        continue
                    print("Found branch point for " + project + ": " + hexsha)
                    revs = revs + tmp_revs
                    break
            except(git.exc.GitCommandError):
                continue

        return revs

    def checkout(self, revision_dict):
        for (repo_name, revision) in revision_dict.items():
            if not revision:
                continue
            repo = self._repos[repo_name]
            lock_file = repo.working_tree_dir + "/.git/index.lock"
            if os.path.exists(lock_file):
                run_batch_command(["rm", "-f", lock_file])
            repo.git.reset("--hard")
            repo.git.checkout(["-f", revision])

class RepoStatus:
    def __init__(self, repos_root=None):
        self.buildspec = ProjectMap().build_spec()
        # key is project, value is repo object
        self._repos = RepoSet(repos_root=repos_root)
        branches = self.buildspec.find("branches")
        self._triggered_repos = {}
        self._branches = []
        for branch in branches.findall("branch"):
            for repo in branch:
                if (repo.attrib.get('branch')
                        and '/' not in repo.attrib.get('branch')):
                    # repo pinned to specific commit
                    continue
                elif ('trigger' in repo.attrib
                        and repo.attrib['trigger'].lower() != "true"):
                    # not triggering repo
                    continue
                if repo.tag not in self._triggered_repos:
                    self._triggered_repos[repo.tag] = None
            try:
                self._branches.append(BranchSpecification(branch,
                                                          repo_set=self._repos))
            except Exception:
                print("WARN: couldn't get status for branch: "
                      + branch.attrib["name"])
                pass
        # referencing the HEAD of an unfetched remote will fail.  This
        # happens the first time branches are polled
        # after. build_specification.xml has been updated to add a
        # remote.
        try:
            self._repos.fetch(only_projects=self._triggered_repos)
        except RepoNotCloned:
            self._repos.clone(limit_to_repos=self._triggered_repos)
            self._repos.fetch(only_projects=self._triggered_repos)

    def poll(self):
        """returns list of branches that should be triggered"""
        ret_dict = {}
        try:
            self._repos.fetch(only_projects=self._triggered_repos)
        except RepoNotCloned:
            self._repos.clone(limit_to_repos=self._triggered_repos)
            self._repos.fetch(only_projects=self._triggered_repos)
        for branch in self._branches:
            trigger_commit = branch.needs_build()
            if trigger_commit:
                ret_dict[branch.name] = trigger_commit
                branch.update_commits(limit_to_repos=self._triggered_repos)
        return ret_dict


class BuildSpecification:
    def __init__(self, repo_set=None):
        self.buildspec = ProjectMap().build_spec()

        if repo_set is None:
            repo_set = RepoSet()
        self._branch_specs = {}

        for abranch in self.buildspec.findall("branches/branch"):
            try:
                branch = BranchSpecification(abranch, repo_set=repo_set)
                self._branch_specs[branch.name] = branch
            except:
                print("WARN: couldn't get status for branch: " + abranch.attrib["name"])
                pass

    def branch_specification(self, branch_name):
        return self._branch_specs[branch_name]

    def checkout(self, branch_name, commits=None):
        if not commits:
            commits = []
        if branch_name in self._branch_specs:
            self._branch_specs[branch_name].checkout()
        else:
            print("WARN: branch not found, ignoring: " + branch_name)
        from . import RevisionSpecification
        rs = RevisionSpecification.from_cmd_line_param(commits)
        rs.checkout()
