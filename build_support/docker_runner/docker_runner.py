#!/usr/bin/env python3
import grp
import os
import platform
import subprocess
import sys
from project_map import ProjectMap
from utils.command import run_batch_command


def get_docker_tag(current_project):
    """ Pull docker_tag value from the build_spec for the given project """

    docker_tag = None
    bs = ProjectMap().build_spec()
    projects = bs.find('projects').findall('project')
    docker_tag = None
    for project in projects:
        if project.attrib['name'] == current_project:
            docker_tag = project.attrib.get('docker_tag')
            break
    return docker_tag


class DockerRunner:
    def __init__(self, container_tag, workdir=None, interactive=False,
                 build_root=None, network=False):
        self._container_tag = container_tag
        self._workdir = workdir
        self._pm = ProjectMap()
        self._build_root = build_root
        if not self._build_root:
            self._build_root = self._pm.build_root()
        self._interactive = interactive
        self._network = network

    def run(self, run_cmd, env=None, workdir=None, rm=True):
        self._env = env
        self._run_cmd = run_cmd
        if not self._env:
            self._env = {}
        if not isinstance(self._run_cmd, list):
            self._run_cmd = [self._run_cmd]
        if workdir:
            self._workdir = workdir
        self._rm = rm

        ld_library_paths = []
        lib_gl_drivers_paths = []
        vk_layer_paths = []
        for arch in ['32', '64']:
            prefix = os.path.join(os.path.dirname(self._build_root), 'm' + arch)
            ld_library_paths += [prefix + "/lib",
                                 prefix + "/lib/dri",
                                 prefix + "/lib" + arch]
            if arch == '64':
                # waffle built on debian installs here...
                ld_library_paths += [
                    prefix + '/lib/x86_64-linux-gnu'
                ]
            lib_gl_drivers_paths += [prefix + "/lib/dri"]
            vk_layer_paths += [prefix + "/lib",
                               prefix + "/share/vulkan/explicit_layer.d/"]
        self._env["LD_LIBRARY_PATH"] = ':'.join(ld_library_paths)
        self._env["LIBGL_DRIVERS_PATH"] = ':'.join(lib_gl_drivers_paths)
        self._env["VK_LAYER_PATH"] = ':'.join(vk_layer_paths)

        # make sure games which use SDL don't try to use anything but x11
        self._env["SDL_VIDEODRIVER"] = "x11"
        self._env["VK_ICD_FILENAMES"] = (self._build_root + '/share/vulkan/icd.d/intel_icd.x86_64.json' +
                                         ':' + self._build_root.replace('m64', 'm32')
                                         + '/share/vulkan/icd.d/intel_icd.i686.json')
        # i965 is not being tested anymore in this CI:
        self._env["MESA_LOADER_DRIVER_OVERRIDE"] = "iris"

        # user needs to be in the video and render groups
        try:
            video_gid = str(grp.getgrnam('video').gr_gid)
            render_gid = str(grp.getgrnam('render').gr_gid)
        except KeyError:
            print("ERROR: Unable to find 'video' group on system.")
            assert(False)
        # pull to get the latest version
        docker_pull_cmd = ['docker', 'pull', self._container_tag]
        try:
            (out, err) = run_batch_command(docker_pull_cmd,
                                           streamedOutput=True, env=self._env)
        except subprocess.CalledProcessError as e:
            print("ERROR: Failed to pull docker tag: " +
                  self._container_tag)
            print(e)

        docker_cmd = ['docker', 'run',
                      '-h', platform.node(),
                      '--ipc', 'host',
                      '-v', '/tmp/.X11-unix:/tmp/.X11-unix:rw',
                      '-v', '/tmp:/tmp:rw',
                      '-v', '/etc/machine-id:/etc/machine-id',
                      '-v', '/dev/shm:/dev/shm',
                      '-v', ('/run/user/' + str(os.getuid()) +
                             ':/run/user/' + str(os.getuid())),
                      '-v', '/dev/dri:/dev/dri:rw',
                      '-v', (os.path.expanduser('~/.Xauthority')
                             + ':/home/steam/.Xauthority'),
                      '-v', '/etc/localtime:/etc/localtime:ro',
                      '-v', '/var/lib/dbus:/var/lib/dbus',
                      '--group-add', video_gid,
                      '--group-add', render_gid,
                      '-e', 'LOCAL_UID=' + str(os.getuid()),
                      '-e', 'LOCAL_GID=' + str(os.getgid()),
                      # needed for vulkan to work in the container
                      '--device', '/dev/dri/renderD128',
                      '--privileged=true',
                      ]
        if self._workdir:
            docker_cmd += ['--workdir', self._workdir]
        if self._rm:
            docker_cmd += ['--rm']
        if self._interactive:
            docker_cmd += ['-it']
        if not self._network:
            docker_cmd += ['--network', 'none']

        # docker accepts env vars with -e, so expand env into parameters
        for k, v in self._env.items():
            docker_cmd += ['-e', k + '=' + v]
        docker_cmd = docker_cmd + [self._container_tag] + self._run_cmd
        out = err = None
        try:
            (out, err) = run_batch_command(docker_cmd, streamedOutput=True,
                                           env=self._env)
        except subprocess.CalledProcessError as e:
            # Handle crashed benchmark checking for gpu hang and quitting
            print("ERROR: Failed to run docker!")
            print(e)
            sys.exit(-1)

        return (out, err)
