#!/bin/bash
# Copyright (C) Intel Corp.  2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/


# Adapted from the script posted here:
# https://github.com/mdaffin/arch-pkgs/blob/master/installer/install-arch

set -uo pipefail
clear
echo "Mesa CI Tester Installer!"

#shellcheck disable=SC2154
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
### Set up logging ###
exec 1> >(tee "/stdout.log")
exec 2> >(tee "/stderr.log")


BASE_PACKAGES="base systemd-swap avahi openssh linux linux-firmware mkinitcpio sudo"
BOOT_SERVICES=("systemd-networkd" "systemd-swap" "avahi-daemon" "sshd" "systemd-resolved")

# Use local mirror
echo "Server = http://linux-ftp.jf.intel.com/pub/mirrors/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist

# find a root disk, ignoring the installation media disk
root_disk=""
mapfile -t all_disks < <(lsblk -dplnx size -o name,LABEL |grep -Ev "boot|loop|ARCH_" | xargs)
media_disk=$(df --output=source -x tmpfs -x devtmpfs|tail -n +2)
#shellcheck disable=SC2068
for disk in ${all_disks[@]}; do
        [[ $media_disk =~ $disk ]] && continue
        root_disk=$disk
        break
done
part_root="${root_disk}2"
part_boot="${root_disk}1"
[ -z "$root_disk" ] && echo "ERROR: No root disk detected!" && exit 1
[ ! -e "$root_disk" ] && echo "ERROR: 'root disk' must be a device file!" && exit 1

# needed for hostname prompt...
pacman -Sy
pacman -S --noconfirm dialog

# Hostname
hostname=$(dialog --stdout --inputbox "Enter hostname" 0 0) || exit 1
clear
#shellcheck disable=SC2086
: ${hostname:?"hostname cannot be empty"}

# Give time to c-C out
echo Using hostname: "$hostname"
echo Starting install in 15 seconds, ctrl-C to cancel...
sleep 15

# is_bios is used to install for GPT/EFI or MBR/BIOS boot
if [ -d /sys/firmware/efi/efivars ]; then
        is_bios=false
else
        is_bios=true
fi

function partition_disk {
        # wipe any existing partitions on the target disk
        wipefs -a "$root_disk"
        if [ "$is_bios" = "true" ]; then
                # Use msdos/mbr partition format for BIOS systems
                parted --script "${root_disk}" -- mklabel msdos \
                        mkpart primary ext2 1Mib 512MiB \
                        set 1 boot on \
                        mkpart primary ext4 513MiB 100%
        else
                # EFI requires gpt partition format and part1 as EFI boot
                # partition
                parted --script "${root_disk}" -- mklabel gpt \
                        mkpart ESP fat32 1Mib 512MiB \
                        set 1 boot on \
                        mkpart primary ext4 513MiB 100%
        fi

        # handle partition block dev naming scheme for nvme
        if [ ! -b $part_boot ]; then
                part_boot=${root_disk}p1
        fi
        if [ ! -b $part_root ]; then
                part_root=${root_disk}p2
        fi
}


function install_bootloader {
        # Grub2 is used for BIOS systems, systemd-boot is used for EFI systems
        if [ "$is_bios" = "true" ]; then
                arch-chroot /mnt pacman --noconfirm -S grub
                arch-chroot /mnt grub-install --target=i386-pc "$root_disk"
                arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
        else
                arch-chroot /mnt bootctl install
                cat >/mnt/boot/loader/entries/arch.conf <<EOF
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=${part_root} rw
EOF

                cat >/mnt/boot/loader/loader.conf <<EOF
default arch
timeout 0
EOF
        fi

}


function setup_services {
        ### Enable services ###
        for service in "${BOOT_SERVICES[@]}"; do
                arch-chroot /mnt systemctl enable "$service"
        done

        ### Configure systemd-swap ###
        mkdir -p /mnt/var/lib/systemd-swap/swapfc/
        arch-chroot /mnt sed -i 's|#swapfc_enabled=0|swapfc_enabled=1|' /etc/systemd/swap.conf
        arch-chroot /mnt sed -i 's|#swapfc_force_preallocated=0|swapfc_force_preallocated=1|' /etc/systemd/swap.conf
}


function setup_filesystems {

        mkfs.ext4 "$part_root"
        # BIOS systems can use ext2 for /boot
        if [ "$is_bios" = "true" ]; then
                mkfs.ext2 "$part_boot"
        # EFI systems require /boot parition to be FAT32
        else
                mkfs.vfat -F32 "$part_boot"
        fi

        mount "$part_root" /mnt
        mkdir /mnt/boot
        mount "$part_boot" /mnt/boot
}


function setup_networking {
        # enable DHCP on all ethernet devices on the system
        # Note: all eth devices seem to start with 'e'.. but this is just an
        # assumption that has worked out so far.
        cat >>/mnt/etc/systemd/network/ethernet.network <<EOF
[Match]
Name=e*

[Network]
DHCP=yes
DNSSEC=no
EOF

        # setup resolve for systemd-resolved
        rm /mnt/etc/resolv.conf
        arch-chroot /mnt ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf

}

### Configure NTP ###
ln -sf /usr/share/zoneinfo/US/Pacific /etc/localtime
timedatectl set-ntp true

partition_disk
setup_filesystems

### Install base system ###
echo "Installing base Arch system"
#shellcheck disable=SC2086
pacstrap /mnt ${BASE_PACKAGES}
genfstab -t PARTUUID /mnt >> /mnt/etc/fstab
echo "$hostname" > /mnt/etc/hostname
cp /etc/pacman.conf /mnt/etc/pacman.conf
echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
arch-chroot /mnt locale-gen

# set up user
echo "Add user for ansible"
arch-chroot /mnt useradd -G wheel -s /bin/bash ansible -m
#shellcheck disable=SC2016
arch-chroot /mnt usermod -p '$6$5hFOSZm3gcp5Jn1Y$WCuyX23qW8CgRLfjdYlkgTdIwRzVaHrvyzkCSWRs4QFsp7xECYt4acWTDQdhSZAs2//Vfw60cPCk04QEgEZji.' ansible
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /mnt/etc/sudoers.d/ansible

echo "Set up ssh for ansible user"
ansible_ssh_home=/home/ansible/.ssh
arch-chroot /mnt mkdir -p $ansible_ssh_home
# keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOv1pOhwqW/+InrpqhAtZODM6B1iDs6YFO1+DOKnKA20 clayton@cacraft-mobl" >> /mnt/$ansible_ssh_home/authorized_keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINJ8pSXgrpxza9dUXI3rOPnEo9OH0ohDvQaiNB48Kf8v clayton@otc-mesa-android" >> /mnt/$ansible_ssh_home/authorized_keys
echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOQafLkAUu2Zyjq+8gDe6dGen2/WGiswniIBR/KIKYoKy83ZGLFcyMhEV19M4JxaziMXL8FtUvkd5tpB7kpbPmA= nico@ngcortes-mobl3" >> /mnt/$ansible_ssh_home/authorized_keys
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK2EDFzdpReCeVGIIVN7M49GPTFoKi9F6l46OE/U5zjj ngcortes@otc-mesa-android" >> /mnt/$ansible_ssh_home/authorized_keys
arch-chroot /mnt chown -R ansible:ansible $ansible_ssh_home
arch-chroot /mnt chmod 700 $ansible_ssh_home


echo "Configuring services and networking"
setup_services
setup_networking

### Install bootloader ###
echo "Installing bootloader"
install_bootloader

# Copy install logs to rootfs
cp /stdout.log /mnt/install_stdout.log 2>/dev/null || true
cp /stderr.log /mnt/install_stderr.log 2>/dev/null || true

umount -R /mnt
echo "Done!"

echo "Shutting down in 30 seconds.. Ctrl-C to abort"
sleep 30
shutdown -h now
